//+------------------------------------------------------------------+
//|                                                       Engima.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct Sequence
  {
   double            a;
   double            b;
   datetime          aTime;
   datetime          bTime;
   int               Trend;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Enigma
  {
private:
   Sequence          sequence;
   void              SetSequence(double a,double b,datetime aTime,datetime bTime,int Trend);
   void              Reset();
   void              newCandleEvent();
   int               SequenceCounter; // Sequence
   int               History;
   int               ActualTrend;
   int               OrderCounter;
   double            ActualLot;
   double            Poin();
   double            PipDiff(double a,double b);
   double            ValidPoint;
   double            ZigZagPoints[];
   datetime          ZigZagPointsTime[];
   double            TriggerPoints[];
   int               OrderTickets[];
   int               GetActualSuperTrendPointColor;
   bool              HaveWeOrderTicket;
   double            T0,T1,nowT0,nowT1;
   datetime          T0D,T1D;
   void              doZigZagPoints();
   void              HorizonLineCreate(color c);
   void              DoSellOrder(double lot);
   void              DoBuyOrder(double lot);
   void              doArrayResizeAndFillDouble(double  &arr[],int index,double value);
   void              doArrayResizeAndFillDate(datetime   &array[],int index,datetime value);
   void              doArrayResizeAndFillString(string   &array[],int index,string value);
   void              doArrayResizeAndFillInt(int   &array[],int index,int value);
   int               getArraySize(double   &array[]);
   bool              HaveWeTrigger;
   bool              HaveWeEnoughtZigZagPoints;
   double            FirstSequenceTreshold;
   double            SecondSequenceTreshold;
   double            ThirdSequenceTreshold;
   double            WeHaveTriggerValue;
   double            GetActualSuperTrendPoint();
   datetime          TimeNow;
   void              DrawLabel(string name,string message,int y,int x=50);
   void              ArrowCreate(string name,int trend);
   void              ArrowLeftPriceCreate(string name);
   int               WhatTrend(double t0,double t1);
   bool              ReinforcedMode;
   void              DoOrderStopLevelModify();
   double            MinStopLevel,Spread,MinStopLevelAndSpread;
public:
                     Enigma();
                    ~Enigma();
   double            GetValidPoint();
   void              doInit(int H);
   void              newCandle();
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Enigma::Enigma()
  {
   ValidPoint=Poin();
   ZeroMemory(ZigZagPoints);
   ZeroMemory(ZigZagPointsTime);
   ZeroMemory(TriggerPoints);
   HaveWeTrigger=false;
   ReinforcedMode=false;
   HaveWeEnoughtZigZagPoints=false;
   FirstSequenceTreshold=40;
   SecondSequenceTreshold=20;
   ThirdSequenceTreshold=40;
   SequenceCounter=0;
   T0=0;
   T1=0;
   nowT0=0;
   nowT1=0;
   ActualLot=0.1;
   OrderCounter=0;
   HaveWeOrderTicket=false;
   GetActualSuperTrendPointColor=0;
   MinStopLevel=MarketInfo(Symbol(),MODE_STOPLEVEL)/10;
   Spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
   MinStopLevelAndSpread=MinStopLevel+Spread;
   Alert("MinStopLevelAndSpread = ",MinStopLevelAndSpread);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Enigma::~Enigma()
  {
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Enigma::Poin()
  {
   int d=Digits;
   switch(d)
     {
      case 2 : {return(Point); break;}
      case 4 : {return(Point); break; }
      case 3 : {return(Point*10); break;}
      case 5 : {return(Point*10); break;}
      default : return(Point);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Enigma::GetValidPoint()
  {
   return ValidPoint;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doInit(int H) // History
  {
   History=H;
   Alert("*** ENIGMA EXPERT ***");
   ArrayResize(ZigZagPoints,1000,1000);
   ArrayResize(ZigZagPointsTime,1000,1000);
   ArrayResize(TriggerPoints,1000,1000);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doZigZagPoints()
  {
   int ZigZagPointCount=0;
   for(int a=0; a<History; a=a+1) // Ciklus ami adatokat gy�jt a gyerty�kb�l.
     {
      double ZigZag=iCustom(NULL,0,"ZigZag",6,5,3,0,a);
      if(ZigZag!=0) // Ha nem 0 az �rt�ke akkor egy �j pont.
        {
         //Alert("<ZIGZAG POINTS>");
         doArrayResizeAndFillDouble(ZigZagPoints,ZigZagPointCount,ZigZag);
         doArrayResizeAndFillDate(ZigZagPointsTime,ZigZagPointCount,Time[a]);
         //Alert("ZigZag = ",ZigZag);
         //Alert("ZigZagPoints[",ZigZagPointCount,"] = ",ZigZagPoints[ZigZagPointCount]);
         //Alert("ZigZagPointsTime[",ZigZagPointCount,"] = ",ZigZagPointsTime[ZigZagPointCount]);           
         ZigZagPointCount++;
         //Alert("[ ZigZagPointCount = ",ZigZagPointCount," ] ");
         //Alert("</ZIGZAG POINTS>");
        }
     }

   if(ZigZagPointCount<=1) // Ha nincs legal�bb k�t pontunk, akkor semmit se csin�lunk.
     {
      HaveWeEnoughtZigZagPoints=false;
     }
   else
     {
      HaveWeEnoughtZigZagPoints=true;
      T0 = ZigZagPoints[0];
      T1 = ZigZagPoints[1];
      T0D = ZigZagPointsTime[0];
      T1D = ZigZagPointsTime[1];
      DrawLabel("T0","T0 = "+string(T0),40);
      DrawLabel("T1","T1 = "+string(T1),60);
      DrawLabel("getArraySize","getArraySize = "+string(getArraySize(ZigZagPoints)),120);
     }
  }
//+------------------------------------------------------------------+
//| ---------------------------------------------------------------- |
//+------------------------------------------------------------------+
void Enigma::newCandleEvent()
  {
   Alert("*** newCandleEvent() ***");
   DoOrderStopLevelModify();
   ActualTrend=WhatTrend(T0,T1); // Megn�z�k most ilyen a trend.
   DrawLabel("ActualTrend","ActualTrend = "+string(ActualTrend),80);
   if(SequenceCounter<1) // Kisebb mint 1, teh�t 0 a
     {
      //Alert("*** (SequenceCounter<1) ***");
      if(PipDiff(T0,T1)>=FirstSequenceTreshold)
        {
         //Alert("*** PipDiff(T0,T1)>=FirstSequenceTreshold ***");
         SetSequence(T0,T1,T0D,T1D,ActualTrend);
         DrawLabel("sequenceTrend","sequence.Trend = "+string(sequence.Trend),100);
         SequenceCounter++;
         ActualLot=0.1;
         //Alert("[ SequenceCounter = ",SequenceCounter," ] ");
         double diff=PipDiff(T0,T1);
         //Alert("[ diff = ",diff," ] ");
         //Alert("[ sequence.Trend = ",sequence.Trend," ] ");
         if(ActualTrend==-1)
           {
            DoBuyOrder(ActualLot);
            //HorizonLineCreate(Green);
           }
         else if(ActualTrend==1)
           {
            DoSellOrder(ActualLot);
            //HorizonLineCreate(Red);
           }
         else { Alert("*** Aktu�lis ActualTrend nem is l�tezhezne: ",ActualTrend," Itt: (PipDiff(T0,T1)>=FirstSequenceTreshold) ","***"); }
        }
      else
        {
         double diff=PipDiff(T0,T1);
         //Alert("[ diff = ",diff," ] ");
        }
     }
   else if(SequenceCounter<=6 && SequenceCounter>=1) // Kisebb vagy egyenl� mint 6 de 1 n�l nagyobb
     {
      //Alert("*** (SequenceCounter<=6 && SequenceCounter>1) ***");

      if(ActualTrend!=sequence.Trend)
        {
         //Alert("*** A trend megt�rt! ***");
         SequenceCounter=0;
         return;
        }
      else
        {
         //Alert("*** A trend folytatodik! ***");
        }
      if(PipDiff(T0,sequence.a)>=SecondSequenceTreshold)
        {
         //Alert("*** (PipDiff(T0,sequence.a)>=SecondSequenceTreshold && ActualTrend==sequence.Trend) ***");
         SetSequence(T0,T1,T0D,T1D,ActualTrend);
         DrawLabel("sequenceTrend","sequence.Trend = "+string(sequence.Trend),100);
         SequenceCounter++;
         ActualLot=ActualLot*SequenceCounter;
         double diff=PipDiff(T0,sequence.a);
         //Alert("[ diff = ",diff," ] ");
         //Alert("[ sequence.Trend = ",sequence.Trend," ] ");
         //Alert("[ SequenceCounter = ",SequenceCounter," ] ");
         if(ActualTrend==-1)
           {
            DoBuyOrder(ActualLot);
            //HorizonLineCreate(Green);
           }
         else if(ActualTrend==1)
           {
            DoSellOrder(ActualLot);
            //HorizonLineCreate(Red);
           }
         else { Alert("*** Aktu�lis ActualTrend nem is l�tezhezne: ",ActualTrend," Itt: (SequenceCounter<=6 && SequenceCounter>=1) ","***"); }
        }
     }
   else if(SequenceCounter==7)
     {
      Alert("*** (SequenceCounter==7) ***");
      if(PipDiff(T0,sequence.a)>=ThirdSequenceTreshold)
        {
         ReinforcedMode=true;
        }
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::newCandle() // Minden �j gyerty�n�l lefut
  {
   if(ReinforcedMode) // Reinforced modban minden le�ll.
     {
      Alert("*** (ReinforcedMode) ***");
      return;
     }
   doZigZagPoints(); // Kellenek a ZigZag pontok
   if(!HaveWeEnoughtZigZagPoints) // Nincs el�g pont
     {
      Alert("*** (!HaveWeEnoughtZigZagPoints) ***");
      Alert("*** Nincs el�g ZigZag pont. ***");
      Alert("[ HaveWeEnoughtZigZagPoints = ",HaveWeEnoughtZigZagPoints," ] ");
      return;
     }
   TimeNow=Time[0]; // Kell a pontos id�

   if(nowT0!=T0 || nowT1!=T1) // Ha v�ltozott a legutobbi t0 vagy t1 �r�tke, teh�t �j ZigZag pont van.
     {
      nowT0=T0;
      nowT1=T1;
      //Alert("*** (nowT0!=T0 || nowT1!=T1) ***");      
      //Alert("[ nowT0 = ",nowT0," ] ");
      //Alert("[ nowT1 = ",nowT1," ] ");
      Alert("[ TimeNow = ",TimeNow," ] ");
      newCandleEvent();
     }

   else //Nem v�ltozott a nowT0 vagy nowT1
     {
      //Alert("*** else ***");
      //Alert("[ T0 = ",T0," ] ");
      //Alert("[ T1 = ",T1," ] ");
      //Alert("[ nowT0 = ",nowT0," ] ");
      //Alert("[ nowT1 = ",nowT1," ] "); 
     }
   Reset();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::Reset()
  {
   ResetLastError();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::SetSequence(double a,double b,datetime aTime,datetime bTime,int Trend)
  {
   sequence.a=a;
   sequence.b=b;
   sequence.aTime=aTime;
   sequence.bTime=bTime;
   sequence.Trend=Trend;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Enigma::WhatTrend(double t0,double t1)
  {
   if(t0<t1)
     {
      return -1; // Cs�kken� trend
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if(t0>t1)
     {
      return 1; // Emelked� trend
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if(t0==t1)
     {
      return 0; // Mivel mind a k�t pont egyenl� j� k�rd�s :D
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else // Ilyen elm�letileg nem lehet, de sose lehet tudni :D
     {
      Alert("S�lyos hiba itt: WhatTrendNow() [ T0 = ",T0," ] ","[ T1 = ",T1," ]");
      return 0;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doArrayResizeAndFillDouble(double   &array[],int index,double value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
//Alert("array[index]=value : ",array[index]=value);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doArrayResizeAndFillDate(datetime   &array[],int index,datetime value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doArrayResizeAndFillString(string   &array[],int index,string value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::doArrayResizeAndFillInt(int   &array[],int index,int value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Enigma::getArraySize(double   &array[])
  {
   return ArraySize(array);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Enigma::PipDiff(double a,double b)
  {
   double value;
   if(a<b) //El kell ker�lni a negativ sz�mokat.
     {
      value=(b-a)/ValidPoint;
     }
   else
     {
      value=(a-b)/ValidPoint;
     }
   return (value);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::DoSellOrder(double lot)
  {
//double TakeProfitLevel=Bid-TakeProfit*vPoint;   //0.0001
   double StopLossLevel=Bid+100*ValidPoint;
   int ticket=OrderSend(Symbol(),OP_SELL,lot,Bid,10,StopLossLevel,NULL,"Megbiz�s k�sz!");
   if(ticket<0)
     {
      int err_code=GetLastError();
      Alert("S�lyos hiba, hibak�d: ",err_code);
     }
   else
     {
      //Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: "+string(ticket));
      OrderCounter++;
      HaveWeOrderTicket=true;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::DoBuyOrder(double lot)
  {
//double TakeProfitLevel=Bid+TakeProfit*ValidPoint;   //0.0001
   double StopLossLevel=Bid-100*ValidPoint;
   int ticket=OrderSend(Symbol(),OP_BUY,lot,Ask,10,StopLossLevel,NULL,"Megbiz�s k�sz!");
   if(ticket<0)
     {
      int err_code=GetLastError();
      Alert("S�lyos hiba, hibak�d: ",err_code);
     }
   else
     {
      //Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: "+string(ticket));
      OrderCounter++;
      HaveWeOrderTicket=true;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::DoOrderStopLevelModify()
  {
   if(HaveWeOrderTicket)
     {
      Alert("*** DoOrderStopLevelModify() ***");
      double ActualTrendPoint=GetActualSuperTrendPoint();
      ActualTrendPoint=NormalizeDouble(ActualTrendPoint,Digits);
      Alert("ActualTrendPoint = ",ActualTrendPoint);
      Alert("OrdersTotal() = ",OrdersTotal());
      for(int i=1; i<=OrdersTotal(); i++)
        {
         bool os=OrderSelect(i-1,SELECT_BY_POS);
         if(!os)
           {
            Alert("Hiba az OrderSelectben. Hiba k�d=",GetLastError());
           }
         else // OrderSelect sikeres.
           {

            if(OrderType()==0 && GetActualSuperTrendPointColor==1 && GetActualSuperTrendPointColor!=0) // OP_BUY
              {
               double MinStop=PipDiff(ActualTrendPoint,OrderOpenPrice());
               Alert("MinStop = ",MinStop);
               Alert("*** OrderType()== 0 ***");
               if(MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** MinStop nagyobb MinStopLevelAndSpread n�l ***");
                 }
               else
                 {
                  Alert("*** MinStop kisebb MinStopLevelAndSpread n�l ***");
                 }
               if(OrderStopLoss()!=ActualTrendPoint && MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** (GetActualSuperTrendPointColor==1 && GetActualSuperTrendPointColor!=0 && OrderStopLoss()!= ActualTrendPoint) ***");
                  bool om=OrderModify(OrderTicket(),OrderOpenPrice(),ActualTrendPoint,NULL,0,Blue);
                  if(!om) { Alert("Hiba az OrderModifyben. Hiba k�d=",GetLastError()); }
                  else { Alert("A megbiz�s modosit�sa sikeres."); }
                 }
              }
            else if(OrderType()==1 && GetActualSuperTrendPointColor==-1 && GetActualSuperTrendPointColor!=0) // OP_SELL
              {
               double MinStop=PipDiff(ActualTrendPoint,OrderOpenPrice());
               Alert("MinStop = ",MinStop);
               Alert("*** OrderType()== 1 ***");

               if(MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** MinStop nagyobb MinStopLevelAndSpread n�l ***");
                 }
               else
                 {
                  Alert("*** MinStop kisebb MinStopLevelAndSpread n�l ***");
                 }
               if(OrderStopLoss()!=ActualTrendPoint && MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** (GetActualSuperTrendPointColor==-1 && GetActualSuperTrendPointColor!=0 && OrderStopLoss()!= ActualTrendPoint) ***");
                  bool om=OrderModify(OrderTicket(),OrderOpenPrice(),ActualTrendPoint,NULL,0,Blue);
                  if(!om) { Alert("Hiba az OrderModifyben. Hiba k�d=",GetLastError()); }
                  else { Alert("A megbiz�s modosit�sa sikeres."); }
                 }
              }
            else // Ilyen nem lehet elm�letileg, de sose lehet tudni.
              {
               break;
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Enigma::GetActualSuperTrendPoint()
  {
   double red=iCustom(NULL,0,"SuperTrendCorrect",10,1,0);
   double green=iCustom(NULL,0,"SuperTrendCorrect",10,0,0);

   if(red>100)
     {
      GetActualSuperTrendPointColor=1;
      return green;
     }
   else
     {
      GetActualSuperTrendPointColor=-1;
      return red;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::HorizonLineCreate(color c)
  {
   string name=string(Time[0])+"L";
   ObjectCreate(name,OBJ_VLINE,0,Time[0],0);
   ObjectSet(name,OBJPROP_COLOR,c);
   ObjectSet(name,OBJPROP_WIDTH,3.0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::DrawLabel(string name,string message,int y,int x=50)
  {
   if(ObjectFind(name)!=-1) ObjectDelete(name);
   ObjectCreate(name,OBJ_LABEL,0,0,0,0,0);
   ObjectSetText(name,message,14,"Verdana",Green);
   ObjectSet(name,OBJPROP_COLOR,Red);
   ObjectSet(name,OBJPROP_CORNER,0);
   ObjectSet(name,OBJPROP_XDISTANCE,x);
   ObjectSet(name,OBJPROP_YDISTANCE,y);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::ArrowCreate(string name,int trend)
  {
   if(ObjectFind(name)<0)
     {
      if(trend==1)
        {
         ObjectCreate(name,OBJ_ARROW_UP,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,5.0);
           }else if(trend==-1){
         ObjectCreate(name,OBJ_ARROW_DOWN,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Red);
         ObjectSet(name,OBJPROP_WIDTH,6.0);

           }else{
         Alert("Hiba az ArrowCreate -ben!");
        }

     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      ObjectDelete(name);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Enigma::ArrowLeftPriceCreate(string name)
  {
   if(ObjectFind(name)<0)
     {
      ObjectCreate(name,OBJ_ARROW_LEFT_PRICE,0,Time[0],Open[0]);
      ObjectSet(name,OBJPROP_COLOR,Green);
      ObjectSet(name,OBJPROP_WIDTH,5.0);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      ObjectDelete(name);
     }
  }
//+------------------------------------------------------------------+
