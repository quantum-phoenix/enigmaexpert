//+------------------------------------------------------------------+
//|                                        EnigmaExpertPrototype.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
extern int History=50; // Menyi gyerty�t n�zzen a m�ltban?
#include "Enigma.mqh"
Enigma ENIGMA;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime LastBarOpenTime;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   ENIGMA.doInit(History);
   Alert("Engima expert elindult!");

   if(!IsConnected())
     {
      Alert("Nincs kapcsolat a szerverrel, a program le�llt.");
     }
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   Alert("Engima expert le�llt!");
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(Bars<History)
     {
      Alert("Nincs el�g gyertya a program le�llt.");
      return;
     }
   if(IsNewBar())
     {
      //Alert("*** �J GYERTYA ***");
      ENIGMA.newCandle();
     }
  }
//+------------------------------------------------------------------+
bool IsNewBar()
  {
   datetime ThisBarOpenTime=Time[0];
   if(ThisBarOpenTime!=LastBarOpenTime)
     {
      LastBarOpenTime=ThisBarOpenTime;
      return (true);
     }
   else
     {
      return(false);
     }
  }
//+------------------------------------------------------------------+
