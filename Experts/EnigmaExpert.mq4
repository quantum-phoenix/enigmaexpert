//+------------------------------------------------------------------+
//|                                                Engima Expert.mq4 |
//|                        Copyright 2014, Rubidium Robotics And Analysis Corp. Ver:0112 |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, Rubidium Controlling And Robotics Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
/*--- Extern v�ltoz�k -------------------------------*/
extern double TakeProfit=50.0;
extern double StopLoss=300.0;
extern double MinTrigger=40.0;
extern double TiggerStep=20.0;
extern int History=50; // Menyi gyerty�t n�zzen a m�ltban?
/*--- Input v�ltoz�k -------------------------------*/
/*--- Arrays -------------------------------*/
double ZigZagPoints[1000];
double Trigger[1000];
datetime TriggerDates[1000];
/*--- V�ltoz�k -------------------------------*/
int StepByStep,Counted_bars, OrderCounter;
datetime lastBarOpenTime, TimeNow;
double ZigZag, StopLevel, Spread, StopLevel_Spread,vPoint,Lot;
/*--- V�ltoz�k  Init -------------------------------*/
int ZigZagPointCount = 0;
int TriggerCount = 0;
bool WeHaveTrigger=false;
int OldTrend = 0;
int NewTrend = 0;
string ObjectArrowName = "Arrow";
int ObjectArrowNamePostfix = 0;
/*--- Debug -------------------------------*/
bool InitDebug = false;
bool TriggerDebug = true;
	bool TriggerTrueDebug = false;
	bool TriggerFalseDebug = true;
bool OrderDebug = false;
bool IndicatorDebug = false;
/*--- OnInit -------------------------------*/  
int OnInit() {
    ResetLastError();
    /*--- Csak egy bizonyos mennyis�g� gyerty�t n�zzen -------------------------------*/
    Counted_bars=IndicatorCounted();
    StepByStep=Bars-Counted_bars-1;
    /*--- Varibles Init -------------------------------*/
    vPoint=Poin(); // Valid Point Init
    TriggerCount = 0;
    OrderCounter = 0;
    TimeNow=Time[0];
    Lot = 0.1;
    WeHaveTrigger = false;
    StopLevel=MarketInfo(Symbol(),MODE_STOPLEVEL)/10;
    Spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
    StopLevel_Spread=StopLevel+Spread;
    if (StopLoss < StopLevel_Spread) StopLoss = StopLevel_Spread;
    if (TakeProfit < StopLevel_Spread) TakeProfit = StopLevel_Spread;
    /*--- Debug -------------------------------*/
    if(InitDebug) { Alert("Az Engima Expert elindult."); }
    if(InitDebug) { Alert("STOPLEVEL: ",StopLevel); }
    if(InitDebug) { Alert("STOPLEVEL+SPREAD: ",StopLevel_Spread); }
    return(INIT_SUCCEEDED);
}
/*--- OnInit -------------------------------*/
void OnDeinit(const int reason) {
    /*--- Debug -------------------------------*/
    if(InitDebug) { Alert("Az Engima Expert bez�rva."); }
}
/*--- OnTick -------------------------------*/
void OnTick() {     
    /*--- Mennyi gyerty�t vizsg�ljak meg? -------------------------------*/
    if(StepByStep>History-1) {
        StepByStep=History-1;
    }
    /*--- Ha van �j b�r, lefut. -------------------------------*/
    if(IsNewBar()) {
	    /*--- ZigZag pontok kisz�m�t�sa. -------------------------------*/        		
        ZigZagCounter();  
	    /*--- Ha nincs k�t pontunk, semmit se csin�lunk tov�bb. -------------------------------*/
        if(ZigZagPointCount<1) {
            if(TriggerDebug) { Alert("Nincs el�g pontunk, ZigZagPointCount: ",ZigZagPointCount); }
            return;
        }
        ObjectArrowNamePostfix++;
        ObjectArrowName = ObjectArrowName + string(ObjectArrowNamePostfix);
        RefreshRates();
        /*--- Debug -------------------------------*/
        if(TriggerDebug) { Alert("// �J GYERTYA ///////////////////////////////// WeHaveTrigger: ",WeHaveTrigger); }  
        /*--- A pontos id�. -------------------------------*/
        TimeNow=Time[0];		
        /*--- Van �j Trigger? -------------------------------*/        
        if(WeHaveTrigger) {
            /*--- Debug -------------------------------*/
            if(TriggerDebug && TriggerTrueDebug) { Alert("<VAN TRIGGER>"); }
            /*--- V�ltoz�k -------------------------------*/
            double lastTriggerPoint = Trigger[TriggerCount];
	        double lastZigZagPoint = ZigZagPoints[0];
            double lastDiff = PipDiff(lastZigZagPoint, lastTriggerPoint);
            /*--- M�r t�bb mint 6 pont van �jraind�tjuk. -------------------------------*/		
            if(TriggerCount>6){ // 
                Alert("T�L SOK A TRIGGER");
                TriggerCount = 0;
                WeHaveTrigger = false;
                return;
            }
            /*--- Milyen a trend? -------------------------------*/
            if(lastZigZagPoint<lastTriggerPoint) { // Cs�kken� trend
                NewTrend = -1;
            }else{
                NewTrend = 1;         
            }
            /*--- Debug -------------------------------*/            
            if(TriggerDebug && TriggerTrueDebug) { Alert("DATE_TRIGGER: " , "?" , " lastTriggerPoint: " , lastTriggerPoint); }
            if(TriggerDebug && TriggerTrueDebug) { Alert("DATE_ZIGZAG: " , TimeNow , " lastZigZagPoint: " , lastZigZagPoint); }
			if(TriggerDebug && TriggerTrueDebug) { Alert("LASTDIFF: ",lastDiff, " TRIGGERCOUNT: ",TriggerCount); }
			if(TriggerDebug && TriggerTrueDebug) { Alert("NEWTREND: " , NewTrend); }
            /*--- Folytatodik a trend? -------------------------------*/
            if(NewTrend == OldTrend) {
                if(TriggerDebug && TriggerTrueDebug) { Alert("A TREND FOLYTATODIK"); }
                /*--- Van 20 pip elmozdul�s? -------------------------------*/
                if(lastDiff>=TiggerStep) {
                    Lot = Lot * 2;
                    TriggerCount++;
                    Trigger[TriggerCount] = lastZigZagPoint;
                    DoOrder(NewTrend*-1,Lot);
                    HorizonLineCreate();
                    /*--- Debug -------------------------------*/
                    if(TriggerDebug && TriggerTrueDebug) { Alert("VAN �J TRIGGER PONT: ", Trigger[TriggerCount]); }
                }
            } else { // A trend v�ltozott
                WeHaveTrigger = false;
				TriggerCount = 0;
            }
            if(TriggerDebug && TriggerTrueDebug) {   Alert("</VAN TRIGGER>"); } 
        }
        /*--- Nincs �j Trigger -------------------------------*/
        else {
            /*--- Debug -------------------------------*/
            if(TriggerDebug && TriggerFalseDebug) { Alert("<NINCS TRIGGER>"); }
            /*--- V�ltoz�k -------------------------------*/
            double Differnce = PipDiff(ZigZagPoints[0],ZigZagPoints[1]);
            Lot = 0.1;
            /*--- Van 40 pip elmozdul�s? -------------------------------*/
            if(Differnce>=MinTrigger) {       
                /*--- T0 �s T1 berak�sa -------------------------------*/			
                ChangeMinTrigger(ZigZagPoints[1],ZigZagPoints[0]);
                /*--- Debug -------------------------------*/
                if(TriggerDebug && TriggerFalseDebug) {                            
                    Alert("T0 DATE: " , TriggerDates[1] , " POINTS: " , Trigger[1] , " PIPDIFFERENCE: " , Differnce);
                    Alert("T1 DATE: " , TriggerDates[0] , " POINTS: " , Trigger[0] ,  " PIPDIFFERENCE: " , Differnce);            
                }            
                WeHaveTrigger = true;
                if(ZigZagPoints[0]<ZigZagPoints[1]) { 
                    OldTrend = -1; // Cs�kken� trend
                    DoOrder(1,Lot); // Pont az ellenkez�j�t kell csin�lni
                    HorizonLineCreate();
                } else {
                    OldTrend = 1;
                    DoOrder(OldTrend*-1,Lot);
                    HorizonLineCreate();
                }
                /*--- Debug -------------------------------*/
                if(TriggerDebug && TriggerFalseDebug) {
                    Alert("OLDTREND: " , OldTrend);                   
                }
            }
            /*--- Debug -------------------------------*/
            if(TriggerDebug && TriggerFalseDebug) { Alert("</NINCS TRIGGER>"); }
        }
    } // IsNewBar
} // OnTick
/*--- Custom Functions -------------------------------*/
bool IsNewBar() {
   datetime thisBarOpenTime=Time[0];
   if(thisBarOpenTime!=lastBarOpenTime)  {
      lastBarOpenTime=thisBarOpenTime;
      return (true);
   }
   else {
      return(false);
   }
}
/*--- InfiniteRun -------------------------------*/
void ZigZagCounter() {
    ZigZagPointCount = 0;
    if(IndicatorDebug) { Alert("<ZIG ZAG COUNTER>"); }    
    for(int a=0; a<StepByStep; a=a+1) { // Ciklus ami adatokat gy�jt a gyerty�kb�l.
        ZigZag=iCustom(NULL,0,"ZigZag",12,5,3,0,a);
		//Alert("ZigZag: ",ZigZag);
        if(ZigZag!=0) { // Ha nem 0 az �rt�ke akkor egy �j pont.
            ZigZagPoints[ZigZagPointCount]=ZigZag; // Az els� 0 a azt�n �gy tov�bb.
			if(IndicatorDebug) { Alert("StepByStep: ", StepByStep, " a: ",a); }
            if(IndicatorDebug) { Alert("Id�pont: ",TriggerDates[ZigZagPointCount]," ZigZagPointCount: ", ZigZagPointCount, " ZigZagPoints[ZigZagPointCount]: ", ZigZagPoints[ZigZagPointCount]); }
            ZigZagPointCount++;
        }
    }
	if(IndicatorDebug) { Alert("</ZIG ZAG COUNTER>"); } 
}
/*--- Trigger Functions -------------------------------*/
void ChangeMinTrigger(double a, double b) {
   Trigger[0] = a;
   Trigger[1] = b;
   TriggerCount = 1; 
}
int TriggerCount() {
   return (ArraySize(Trigger));
}
/*--- Math formulas -------------------------------*/
double Poin() {
   int d=Digits;
   switch(d){
      case 2 : {return(Point); break;}
      case 4 : {return(Point); break; }
      case 3 : {return(Point*10); break;}
      case 5 : {return(Point*10); break;}
      default : return(Point);
   }

}
double PipDiff(double a,double b) {
   double value;
   if(a<b) { //El kell ker�lni a negativ sz�mokat.     
      value=(b-a)/vPoint;
   }
   else {
      value=(a-b)/vPoint;
   }
   return (value);
}
void DoOrder(int trend, double lot) {
    if(trend == 1){
		double TakeProfitLevel=Bid+TakeProfit*vPoint;   //0.0001
		double StopLossLevel=Bid-StopLoss*vPoint;
		if(OrderDebug) {  	
			Alert("DATE: ",TimeNow, " TAKEPROFITLEVEL = ", TakeProfitLevel);
			Alert("DATE: ",TimeNow, " STOPLOSSLEVEL = ", StopLossLevel);
			Alert("DATE: ",TimeNow, " Bid = ", Bid);
			Alert("DATE: ",TimeNow, " Ask = ", Ask);
			Alert("DATE: ",TimeNow, " Trend = ", trend);
		}
		int ticket;			   
		ticket = OrderSend(Symbol(), OP_BUY, lot, Ask, 10, StopLossLevel, TakeProfitLevel, "Megbiz�s sosz�ma: " + string(OrderCounter));
		if(ticket < 0)
		{
			int err_code=GetLastError();
			Alert("S�lyos hiba, hibak�d: " , err_code);
		}
		else {
			OrderCounter++;
			Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: " + string(ticket)); 
		}
	} else if(trend == -1) {
		double TakeProfitLevel=Bid-TakeProfit*vPoint;   //0.0001
		double StopLossLevel=Bid+StopLoss*vPoint;
		if(OrderDebug) {  	
			Alert("DATE: ",TimeNow, " TAKEPROFITLEVEL = ", TakeProfitLevel);
			Alert("DATE: ",TimeNow, " STOPLOSSLEVEL = ", StopLossLevel);
			Alert("DATE: ",TimeNow, " Bid = ", Bid);
			Alert("DATE: ",TimeNow, " Ask = ", Ask);
			Alert("DATE: ",TimeNow, " Trend = ", trend);
		}
		int ticket;			   
		ticket = OrderSend(Symbol(), OP_SELL, lot, Bid, 10, StopLossLevel, TakeProfitLevel, "Megbiz�s sosz�ma: " + string(OrderCounter));
		if(ticket < 0)
		{
			int err_code=GetLastError();
			Alert("S�lyos hiba, hibak�d: " , err_code);
		}
		else {
			OrderCounter++;
			Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: " + string(ticket)); 
		}
	} else {
		if(OrderDebug) { Alert("S�ly�s hiba a DoTrend -ben, a trend: ",string(trend)); }
	}
	
}
void ArrowCreate(string name,int trend)
  {
   if(ObjectFind(name)<0)
     {
      if(trend==1)
        {
		 WindowRedraw();
         ObjectCreate(name,OBJ_ARROW_UP,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,5.0);
           }else if(trend==-1){
         ObjectCreate(name,OBJ_ARROW_DOWN,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Red);
         ObjectSet(name,OBJPROP_WIDTH,6.0);

           }else{
         Alert("Hiba az ArrowCreate -ben!");
        }

     }
   else
     {
      ObjectDelete(name);
     }
  }
  
  void HorizonLineCreate()
  {
         string name = string(Time[0]);
         ObjectCreate(name,OBJ_VLINE,0,Time[0],0);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,3.0);
  }