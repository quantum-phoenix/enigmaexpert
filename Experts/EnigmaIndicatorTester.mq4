//+------------------------------------------------------------------+
//|                                                Engima Expert.mq4 |
//|    Copyright 2014, Rubidium Robotics And Analysis Corp. Ver:0112 |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

extern int History=50; // Menyi gyerty�t n�zzen a m�ltban?

datetime lastBarOpenTime, TimeNow;
int StepByStep,Counted_bars, OrderCounter;
double SST;
int OnInit(){
    SST = 0;
    /*--- Csak egy bizonyos mennyis�g� gyerty�t n�zzen -------------------------------*/
    Counted_bars=IndicatorCounted();
    StepByStep=Bars-Counted_bars-1;
    return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason) {}

void OnTick() {
    /*--- Mennyi gyerty�t vizsg�ljak meg? -------------------------------*/
    if(StepByStep>History-1) {
        StepByStep=History-1;
    }
    /*--- Ha van �j b�r, lefut. -------------------------------*/
    if(IsNewBar()) {
        SuperTrendIndicator();
        Alert("�j bar!");
        Alert("SST = " , SST);
    }
}
bool IsNewBar() {
   datetime thisBarOpenTime=Time[0];
   if(thisBarOpenTime!=lastBarOpenTime)  {
      lastBarOpenTime=thisBarOpenTime;
      return (true);
   }
   else {
      return(false);
   }
}
void SuperTrendIndicator() {
    int shift = 0;
    Alert("Shift = ",shift);
    SST=iCustom(NULL,0,"SuperTrendCorrect",50,0,shift);    
}
