//+------------------------------------------------------------------+
//|                                                  EngimaCloud.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

extern int Expert_ID=666;
extern int History=50;
extern double FirstSequenceTreshold=40.0;
extern double SecondSequenceTreshold=20.0;
extern double ThirdSequenceTreshold=20.0;
int Magic_Number,GetActualSuperTrendPointColor,SequenceCounter;
double Valid_Point,T0,T1,nowT0,nowT1,ActualLot,MinStopLevel,Spread,MinStopLevelAndSpread;
datetime T0D,T1D,TimeNow;
double ZigZagPoints[];
double TriggerPoints[];
datetime ZigZagPointsTime[];
bool ReinforcedMode;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct Sequence
  {
   double            a;
   double            b;
   datetime          aTime;
   datetime          bTime;
   int               trend;
  };
Sequence sequence;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   Alert("Az Engima Cloud Expert elindult!");
//+--- Sz�ks�ges v�ltozok
   SequenceCounter=0;
   ActualLot=0.0;
   MinStopLevel=MarketInfo(Symbol(),MODE_STOPLEVEL)/10;
   Spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
   MinStopLevelAndSpread=MinStopLevel+Spread;
//+--- Magic Number r�sz
   int Period_ID=0;
   switch(Period())
     {
      case PERIOD_MN1: Period_ID = 9; break;
      case PERIOD_W1:  Period_ID = 8; break;
      case PERIOD_D1:  Period_ID = 7; break;
      case PERIOD_H4:  Period_ID = 6; break;
      case PERIOD_H1:  Period_ID = 5; break;
      case PERIOD_M30: Period_ID = 4; break;
      case PERIOD_M15: Period_ID = 3; break;
      case PERIOD_M5:  Period_ID = 2; break;
      case PERIOD_M1:  Period_ID = 1; break;
     }
   Magic_Number=Expert_ID*10+Period_ID;
//+--- Valid pont
   Valid_Point=Poin();
//+--- Reinforced Mode ha aktiv semmi se fut le, olyan mint egy v�szkapcsol�
   ReinforcedMode=false;
//+--- Array init m�veletek
   ArrayResize(ZigZagPoints,100,100);
   ArrayResize(ZigZagPointsTime,100,100);
   ArrayResize(TriggerPoints,100,100);
//+--- Template bet�lt�se, hogy ne k�zzel kelljen
   TemplateLoad();
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   Alert("Az Engima Cloud Expert le�llt!");
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   Reset(); // Minden �j tick el�tt kell egy reset. K�s�bb k�nnyen b�vithet�
   newTick(); // �j tick
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Poin() // Kisz�molja a validpontokat, teljesen korrekt megold�s
  {
   int d=Digits;
   switch(d)
     {
      case 2 : {return(Point); break;}
      case 4 : {return(Point); break; }
      case 3 : {return(Point*10); break;}
      case 5 : {return(Point*10); break;}
      default : return(Point);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool doZigZagPoints() // Visszaadja a k�t utols� ZigZag pontot.
  {
   int ZigZagPointCount=0;
   bool HaveWeEnoughtZigZagPoints=false;
   for(int a=0; a<History; a=a+1) // Ciklus ami adatokat gy�jt a gyerty�kb�l.
     {
      double ZigZag=iCustom(NULL,0,"ZigZag",6,5,3,0,a);
      if(ZigZag!=0) // Ha nem 0 az �rt�ke akkor egy �j pont.
        {
         doArrayResizeAndFillDouble(ZigZagPoints,ZigZagPointCount,ZigZag);
         doArrayResizeAndFillDate(ZigZagPointsTime,ZigZagPointCount,Time[a]);
         ZigZagPointCount++;
        }
     }
   if(ZigZagPointCount<=1) // Ha nincs legal�bb k�t pontunk, akkor semmit se csin�lunk.
     {
      HaveWeEnoughtZigZagPoints=false;
     }
   else
     {
      T0 = ZigZagPoints[0];
      T1 = ZigZagPoints[1];
      T0D = ZigZagPointsTime[0];
      T1D = ZigZagPointsTime[1];
      HaveWeEnoughtZigZagPoints=true;
     }
   return HaveWeEnoughtZigZagPoints;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double PipDiff(double a,double b)
  {
   if(a<b) //El kell ker�lni a negativ sz�mokat.
     {
      return((b-a)/Valid_Point);
     }
   else
     {
      return((a-b)/Valid_Point);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void HorizonLineCreate(color c)
  {
   string name=string(Time[0])+"L";
   ObjectCreate(name,OBJ_VLINE,0,Time[0],0);
   ObjectSet(name,OBJPROP_COLOR,c);
   ObjectSet(name,OBJPROP_WIDTH,3.0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DrawLabel(string name,string message,int y,int x=50)
  {
   if(ObjectFind(name)!=-1) ObjectDelete(name);
   ObjectCreate(name,OBJ_LABEL,0,0,0,0,0);
   ObjectSetText(name,message,14,"Verdana",Green);
   ObjectSet(name,OBJPROP_COLOR,Red);
   ObjectSet(name,OBJPROP_CORNER,0);
   ObjectSet(name,OBJPROP_XDISTANCE,x);
   ObjectSet(name,OBJPROP_YDISTANCE,y);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ArrowCreate(string name,int trend)
  {
   if(ObjectFind(name)<0)
     {
      if(trend==1)
        {
         ObjectCreate(name,OBJ_ARROW_UP,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,5.0);
           }else if(trend==-1){
         ObjectCreate(name,OBJ_ARROW_DOWN,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Red);
         ObjectSet(name,OBJPROP_WIDTH,6.0);

           }else{
         Alert("Hiba az ArrowCreate -ben!");
        }
     }
   else
     {
      ObjectDelete(name);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ArrowLeftPriceCreate(string name)
  {
   if(ObjectFind(name)<0)
     {
      ObjectCreate(name,OBJ_ARROW_LEFT_PRICE,0,Time[0],Open[0]);
      ObjectSet(name,OBJPROP_COLOR,Green);
      ObjectSet(name,OBJPROP_WIDTH,5.0);
     }
   else
     {
      ObjectDelete(name);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void doArrayResizeAndFillDouble(double   &array[],int index,double value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
//Alert("array[index]=value : ",array[index]=value);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void doArrayResizeAndFillDate(datetime   &array[],int index,datetime value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void doArrayResizeAndFillString(string   &array[],int index,string value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void doArrayResizeAndFillInt(int   &array[],int index,int value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,1000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DoSellOrder(double lot)
  {
//double TakeProfitLevel=Bid-TakeProfit*vPoint;   //0.0001
   double StopLossLevel=Bid+100*Valid_Point;
   int ticket=OrderSend(Symbol(),OP_SELL,lot,Bid,5,StopLossLevel,NULL,"Megbiz�s k�sz!",Magic_Number,0,Red);
   if(ticket<0)
     {
      int err_code=GetLastError();
      Alert("S�lyos hiba, hibak�d: ",err_code);
     }
   else
     {
      Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: "+string(ticket));
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DoBuyOrder(double lot)
  {
//double TakeProfitLevel=Bid+TakeProfit*ValidPoint;   //0.0001
   double StopLossLevel=Bid-100*Valid_Point;
   int ticket=OrderSend(Symbol(),OP_BUY,lot,Ask,5,StopLossLevel,NULL,"Megbiz�s k�sz!",Magic_Number,0,Red);
   if(ticket<0)
     {
      int err_code=GetLastError();
      Alert("S�lyos hiba, hibak�d: ",err_code);
     }
   else
     {
      Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: "+string(ticket));
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double GetActualSuperTrendPoint()
  {
   double red=iCustom(NULL,0,"SuperTrendCorrect",10,1,0);
   double green=iCustom(NULL,0,"SuperTrendCorrect",10,0,0);

   if(red>100) // Ez a 100 egy hat�r�rt�k, el�g h�ly�n van megcsin�lva az indik�tor. Valami 20000 es kordin�t�ra rakja a nem aktu�lis pontot
     {
      GetActualSuperTrendPointColor=1;
      return green;
     }
   else
     {
      GetActualSuperTrendPointColor=-1;
      return red;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Reset()
  {
   ResetLastError();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int WhatTrend(double t0,double t1)
  {
   if(t0<t1)
     {
      return -1; // Cs�kken� trend
     }
   else if(t0>t1)
     {
      return 1; // Emelked� trend
     }
   else if(t0==t1)
     {
      return 0; // Mivel mind a k�t pont egyenl� j� k�rd�s :D
     }
   else // Ilyen elm�letileg nem lehet, de sose lehet tudni :D
     {
      Alert("S�lyos hiba itt: WhatTrendNow() [ T0 = ",T0," ] ","[ T1 = ",T1," ]");
      return 0;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void newTick() // Minden �j gyerty�n�l lefut
  {
   if(Bars<History)
     {
      Alert("Nincs el�g gyertya a program le�llt.");
      return;
     }
   if(ReinforcedMode) // Reinforced modban minden le�ll.
     {
      Alert("*** (ReinforcedMode) ***");
      return;
     }
   if(!doZigZagPoints()) // Nincs el�g pont
     {
      Alert("*** Nincs el�g ZigZag pont. ***");
      return;
     }
     else // De ha van el�g pont
     {
      TimeNow=Time[0]; // Kell a pontos id�
      if(nowT0!=T0 || nowT1!=T1) // Ha v�ltozott a legutobbi t0 vagy t1 �r�tke, teh�t �j ZigZag pont van.
        {
         nowT0=T0;
         nowT1=T1;
         newTickEvent();
        }
     }
   Reset();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void newTickEvent()
  {
   //DoOrderStopLevelModify();
   int ActualTrend=WhatTrend(T0,T1); // Megn�z�k most ilyen a trend.
   DrawLabel("ActualTrend","ActualTrend = "+string(ActualTrend),80);

   if(SequenceCounter<1) // Kisebb mint 1, teh�t 0 a
     {
      //Alert("*** (SequenceCounter<1) ***");
      if(PipDiff(T0,T1)>=FirstSequenceTreshold)
        {
         //Alert("*** PipDiff(T0,T1)>=FirstSequenceTreshold ***");
         SetSequence(T0,T1,T0D,T1D,ActualTrend);
         DrawLabel("sequenceTrend","sequence.Trend = "+string(sequence.trend),100);
         SequenceCounter++;
         ActualLot=0.1;
         //Alert("[ SequenceCounter = ",SequenceCounter," ] ");
         double diff=PipDiff(T0,T1);
         //Alert("[ diff = ",diff," ] ");
         //Alert("[ sequence.Trend = ",sequence.Trend," ] ");
         if(ActualTrend==-1)
           {
            DoBuyOrder(ActualLot);
            //HorizonLineCreate(Green);
           }
         else if(ActualTrend==1)
           {
            DoSellOrder(ActualLot);
            //HorizonLineCreate(Red);
           }
         else { Alert("*** Aktu�lis ActualTrend nem is l�tezhezne: ",ActualTrend," Itt: (PipDiff(T0,T1)>=FirstSequenceTreshold) ","***"); }
        }
      else
        {
         double diff=PipDiff(T0,T1);
         //Alert("[ diff = ",diff," ] ");
        }
     }
   else if(SequenceCounter<=6 && SequenceCounter>=1) // Kisebb vagy egyenl� mint 6 de 1 n�l nagyobb
     {
      //Alert("*** (SequenceCounter<=6 && SequenceCounter>1) ***");

      if(ActualTrend!=sequence.trend)
        {
         //Alert("*** A trend megt�rt! ***");
         SequenceCounter=0;
         return;
        }
      else
        {
         //Alert("*** A trend folytatodik! ***");
        }
      if(PipDiff(T0,sequence.a)>=SecondSequenceTreshold)
        {
         //Alert("*** (PipDiff(T0,sequence.a)>=SecondSequenceTreshold && ActualTrend==sequence.Trend) ***");
         SetSequence(T0,T1,T0D,T1D,ActualTrend);
         DrawLabel("sequenceTrend","sequence.Trend = "+string(sequence.trend),100);
         SequenceCounter++;
         ActualLot=ActualLot*SequenceCounter;
         double diff=PipDiff(T0,sequence.a);
         //Alert("[ diff = ",diff," ] ");
         //Alert("[ sequence.Trend = ",sequence.Trend," ] ");
         //Alert("[ SequenceCounter = ",SequenceCounter," ] ");
         if(ActualTrend==-1)
           {
            DoBuyOrder(ActualLot);
            //HorizonLineCreate(Green);
           }
         else if(ActualTrend==1)
           {
            DoSellOrder(ActualLot);
            //HorizonLineCreate(Red);
           }
         else { Alert("*** Aktu�lis ActualTrend nem is l�tezhezne: ",ActualTrend," Itt: (SequenceCounter<=6 && SequenceCounter>=1) ","***"); }
        }
     }
   else if(SequenceCounter==7)
     {
      Alert("*** (SequenceCounter==7) ***");
      if(PipDiff(T0,sequence.a)>=ThirdSequenceTreshold)
        {
         ReinforcedMode=true;
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SetSequence(double a,double b,datetime aTime,datetime bTime,int trend)
  {
   sequence.a=a;
   sequence.b=b;
   sequence.aTime=aTime;
   sequence.bTime=bTime;
   sequence.trend=trend;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DoOrderStopLevelModify()
  {
   if(OrdersTotal()>0) // Ha van megbiz�s
     {
      Alert("*** DoOrderStopLevelModify() ***");
      double ActualTrendPoint=GetActualSuperTrendPoint();
      ActualTrendPoint=NormalizeDouble(ActualTrendPoint,Digits);
      Alert("ActualTrendPoint = ",ActualTrendPoint);
      Alert("OrdersTotal() = ",OrdersTotal());
      for(int i=1; i<=OrdersTotal(); i++)
        {
         bool os=OrderSelect(i-1,SELECT_BY_POS);
         if(!os)
           {
            Alert("Hiba az OrderSelectben. Hiba k�d=",GetLastError());
           }
         else // OrderSelect sikeres.
           {

            if(OrderType()==0 && GetActualSuperTrendPointColor==1 && GetActualSuperTrendPointColor!=0) // OP_BUY
              {
               double MinStop=PipDiff(ActualTrendPoint,OrderOpenPrice());
               Alert("MinStop = ",MinStop);
               Alert("*** OrderType()== 0 ***");
               if(MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** MinStop nagyobb MinStopLevelAndSpread n�l ***");
                 }
               else
                 {
                  Alert("*** MinStop kisebb MinStopLevelAndSpread n�l ***");
                 }
               if(OrderStopLoss()!=ActualTrendPoint && MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** (GetActualSuperTrendPointColor==1 && GetActualSuperTrendPointColor!=0 && OrderStopLoss()!= ActualTrendPoint) ***");
                  bool om=OrderModify(OrderTicket(),OrderOpenPrice(),ActualTrendPoint,NULL,0,Blue);
                  if(!om) { Alert("Hiba az OrderModifyben. Hiba k�d=",GetLastError()); }
                  else { Alert("A megbiz�s modosit�sa sikeres."); }
                 }
              }
            else if(OrderType()==1 && GetActualSuperTrendPointColor==-1 && GetActualSuperTrendPointColor!=0) // OP_SELL
              {
               double MinStop=PipDiff(ActualTrendPoint,OrderOpenPrice());
               Alert("MinStop = ",MinStop);
               Alert("*** OrderType()== 1 ***");

               if(MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** MinStop nagyobb MinStopLevelAndSpread n�l ***");
                 }
               else
                 {
                  Alert("*** MinStop kisebb MinStopLevelAndSpread n�l ***");
                 }
               if(OrderStopLoss()!=ActualTrendPoint && MinStop>=MinStopLevelAndSpread)
                 {
                  Alert("*** (GetActualSuperTrendPointColor==-1 && GetActualSuperTrendPointColor!=0 && OrderStopLoss()!= ActualTrendPoint) ***");
                  bool om=OrderModify(OrderTicket(),OrderOpenPrice(),ActualTrendPoint,NULL,0,Blue);
                  if(!om) { Alert("Hiba az OrderModifyben. Hiba k�d=",GetLastError()); }
                  else { Alert("A megbiz�s modosit�sa sikeres."); }
                 }
              }
            else // Ilyen nem lehet elm�letileg, de sose lehet tudni.
              {
               break;
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void TemplateLoad()
  {
   if(!ChartApplyTemplate(ChartID(),"Enigma")) // Ha nincs rajta a template
     {
      Print("Nem tudom bet�lteni az Engima template f�jlt, hiba: ",GetLastError());
      ResetLastError();
      if(FileIsExist("Enigma"))
        {
         Print("Az Enigma template f�jl l�tezik, de nincs .tpl kiterjeszt�se.");
        }
      else
        {
         Print("'Enigma.tpl' nem tal�lom, pedig itt k�ne lennie "+TerminalInfoString(TERMINAL_PATH)+"\\MQL4\\Files");
        }
      if(ChartApplyTemplate(ChartID(),"Enigma.tpl"))
        {
         Print("'Enigma.tpl' template f�jl sikeresen bet�ltve.");
        }
      else
        {
         Print("Nem tudom bet�lteni az Engima template f�jlt, hiba: ",GetLastError());
        }
     }
   else
     {
      Print("'Enigma' template sikeresen bet�ltve kiterjeszt�s n�lk�l.");
     }
  }
//+------------------------------------------------------------------+
