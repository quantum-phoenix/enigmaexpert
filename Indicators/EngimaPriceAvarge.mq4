//+------------------------------------------------------------------+
//|                                            EngimaPriceAvarge.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 1 // Number of buffers
#property indicator_color1 Orange // Color of the 1st line
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+

double BufferZero[];
double AvargePrice,AllBuyOrderAvarge,AllSellOrderAvarge;
int AllOrdersTotal,AllSellOrdersTotal,AllBuyOrdersTotal;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   AllOrdersTotal = AllSellOrdersTotal = AllBuyOrdersTotal = 0;
   SetIndexBuffer(0,BufferZero); // Assigning an array to a buffer
   SetIndexStyle(0,DRAW_LINE,STYLE_DOT,2);// Line style
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   int i,// Bar index
   Counted_bars; // Number of counted bars
   Counted_bars=IndicatorCounted(); // Number of counted bars
   i=Bars-Counted_bars-1; // Index of the first uncounted
   MakeAvargePrice();
   while(i>=0) // Loop for uncounted bars
     {
      if(AvargePrice==0)
        {
         BufferZero[i]=Open[i];
        }
      else
        {
         BufferZero[i]=AvargePrice;
        }
      i--;
     }
   return(rates_total);
  }
//+------------------------------------------------------------------+
void MakeAvargePrice()
  {
   AvargePrice=AllBuyOrderAvarge=AllSellOrderAvarge=0;
   Alert("OrdersTotal() = ",OrdersTotal());
   for(int k=1; k<=OrdersTotal(); k++)
     {
      bool os=OrderSelect(k-1,SELECT_BY_POS);
      if(!os)
        {
         Alert("Hiba az OrderSelectben. Hiba k�d=",GetLastError());
         ResetLastError();
        }
      else // OrderSelect sikeres.
        {

         if(OrderType()==0)// OP_BUY
           {
            AllBuyOrdersTotal++;
            AllBuyOrderAvarge=AllBuyOrderAvarge+OrderOpenPrice();
            //Alert("AllBuyOrderAvarge = ",AllBuyOrderAvarge);
           }
         if(OrderType()==1)// OP_SELL
           {
            AllSellOrdersTotal++;
            AllSellOrderAvarge=AllSellOrderAvarge+OrderClosePrice();
            //Alert("AllSellOrderAvarge = ",AllSellOrderAvarge);
           }
        }
     }
   if(AllBuyOrdersTotal!=0)
     {
      AvargePrice=(AllBuyOrderAvarge/AllBuyOrdersTotal)/1;
     }
   else if(AllSellOrdersTotal!=0)
     {
      AvargePrice=(AllSellOrderAvarge/AllSellOrdersTotal)/1;
     }
   else if(AllBuyOrdersTotal!=0 && AllSellOrdersTotal!=0)
     {
      AvargePrice=((AllBuyOrderAvarge/AllBuyOrdersTotal)+(AllSellOrderAvarge/AllSellOrdersTotal))/2;
     }
   else
     {
      AvargePrice=0;
     }
  }
//+------------------------------------------------------------------+
