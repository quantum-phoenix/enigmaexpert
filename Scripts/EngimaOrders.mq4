//+------------------------------------------------------------------+
//|                                                    OrderTest.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
extern double TakeProfit=50.0;
extern double StopLoss=40.0;
double vPoint;
/*--- Debug -------------------------------*/
bool InitDebug = false;
bool TriggerDebug = false;
bool OrderDebug = true;
bool IndicatorDebug = false;
datetime TimeNow;
int OrderCounter;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   TimeNow=Time[0];
   vPoint=Poin(); // Valid Point Init
   RefreshRates();
   Alert("////////////////////////////");
   Alert("Bid: ",Bid);
   Alert("Ask: ",Ask);
   Alert("Point: ",Point); //0.00001
   Alert("***************************");
/****************************************************/
   double bid =MarketInfo(Symbol(),MODE_BID); // Request for the value of Bid
   double ask =MarketInfo(Symbol(),MODE_ASK); // Request for the value of Ask
   double point=MarketInfo(Symbol(),MODE_POINT);//Request for Point
   Alert("bid: ",bid);
   Alert("ask: ",ask);
   Alert("point: ",point); //0.00001
/****************************************************/
   double TakeProfitLevel=Bid+TakeProfit*Point;   //0.0001
   double StopLossLevel=Bid-StopLoss*Point;
   Alert("TakeProfitLevel: ",TakeProfitLevel);
   Alert("StopLossLevel: ",StopLossLevel);
/****************************************************/
   double StopLevel=MarketInfo(Symbol(),MODE_STOPLEVEL)/10;
   double Spread=MarketInfo(Symbol(),MODE_SPREAD)/10;
   double StopLevel_Spread=StopLevel+Spread;
   Alert("STOPLEVEL: ",StopLevel);
   Alert("SPREAD: ",Spread);
   Alert("STOPLEVEL+SPREAD: ",StopLevel_Spread);
/****************************************************/
   if (StopLoss < StopLevel_Spread) StopLoss = StopLevel_Spread;
   if (TakeProfit < StopLevel_Spread) TakeProfit = StopLevel_Spread;
  DoOrder(1); // 1 = Buy, -1 = Sell
  }
//+------------------------------------------------------------------+
void DoOrder(int trend) {
    if(trend == 1){
		double TakeProfitLevel=Bid+TakeProfit*vPoint;   //0.0001
		double StopLossLevel=Bid-StopLoss*vPoint;
		if(OrderDebug) {  	
			Alert("DATE: ",TimeNow, " TAKEPROFITLEVEL = ", TakeProfitLevel);
			Alert("DATE: ",TimeNow, " STOPLOSSLEVEL = ", StopLossLevel);
			Alert("DATE: ",TimeNow, " Bid = ", Bid);
			Alert("DATE: ",TimeNow, " Ask = ", Ask);
			Alert("DATE: ",TimeNow, " Trend = ", trend);
		}
		int ticket;			   
		ticket = OrderSend(Symbol(), OP_BUY, 0.1, Ask, 10, StopLossLevel, TakeProfitLevel, "Megbiz�s sosz�ma: " + string(OrderCounter));
		if(ticket < 0)
		{
			int err_code=GetLastError();
			Alert("S�lyos hiba, hibak�d: " , err_code);
		}
		else {
			OrderCounter++;
			Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: " + string(ticket)); 
		}
	} else if(trend == -1) {
		double TakeProfitLevel=Bid-TakeProfit*vPoint;   //0.0001
		double StopLossLevel=Bid+StopLoss*vPoint;
		if(OrderDebug) {  	
			Alert("DATE: ",TimeNow, " TAKEPROFITLEVEL = ", TakeProfitLevel);
			Alert("DATE: ",TimeNow, " STOPLOSSLEVEL = ", StopLossLevel);
			Alert("DATE: ",TimeNow, " Bid = ", Bid);
			Alert("DATE: ",TimeNow, " Ask = ", Ask);
			Alert("DATE: ",TimeNow, " Trend = ", trend);
		}
		int ticket;			   
		ticket = OrderSend(Symbol(), OP_SELL, 0.1, Ask, 10, StopLossLevel, TakeProfitLevel, "Megbiz�s sosz�ma: " + string(OrderCounter));
		if(ticket < 0)
		{
			int err_code=GetLastError();
			Alert("S�lyos hiba, hibak�d: " , err_code);
		}
		else {
			OrderCounter++;
			Alert("Az �j megbiz�s l�trej�tt, az azonosit�ja: " + string(ticket)); 
		}
	} else {
		if(OrderDebug) { Alert("S�ly�s hiba a DoTrend -ben, a trend: ",string(trend)); }
	}
	
}

double Poin() {
   int d=Digits;
   switch(d){
      case 2 : {return(Point); break;}
      case 4 : {return(Point); break; }
      case 3 : {return(Point*10); break;}
      case 5 : {return(Point*10); break;}
      default : return(Point);
   }

}