//+------------------------------------------------------------------+
//|                                           EnigmaExperimental.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
double arr[];
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   Alert("*** EXPERIMENTAL ***");
   int Counted_bars=IndicatorCounted();
   int StepByStep=Bars-Counted_bars-1;
   Alert("Counted_bars = ",Counted_bars);
   Alert("StepByStep = ",StepByStep);
   Alert("Bars = ", Bars);
   ArrayResize(arr,100,100);
   DoArrayResize(arr,0,3.14);
   Alert(arr[0]+1);
  }
//+------------------------------------------------------------------+
void DoArrayResize(double  &array[],int index,double value)
  {
   int indexIncrement=index+1;
   ArrayResize(array,indexIncrement,100000);
   array[index]=value;
  }
//+------------------------------------------------------------------+
