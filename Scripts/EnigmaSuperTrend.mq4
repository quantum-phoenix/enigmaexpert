//+------------------------------------------------------------------+
//|                                             EnigmaSuperTrend.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
int sti_arr[100];
int History=30;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   Alert("*** STI ***");
   for(int a=0; a<History; a=a+1)
     {
      double sti=iCustom(NULL,0,"SuperTrendCorrect",10,1,a);
      double sti2=iCustom(NULL,0,"SuperTrendCorrect",10,0,a);
      if(sti>100)
        {
         Alert("STI2 = ",sti2);
        }
      else
        {
         Alert("STI = ",sti);
        }
     }
  }
//+------------------------------------------------------------------+
