//+------------------------------------------------------------------+
//|                                           EngimaCalculations.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

double vPoint;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   RefreshRates();
   Alert("////////////////////////////");
   Alert("Bid: ",Bid);
   Alert("Ask: ",Ask);
   Alert("Point: ",Point); //0.00001
   Alert("***************************");
/****************************************************/
   vPoint=Poin();
   Alert("vPoint: ",vPoint);
   Alert(PipDiff(Bid,Ask));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double Poin()
  {
   int d=Digits;
   switch(d)
     {
      case 2 : {return(Point); break;}
      case 4 : {return(Point); break; }
      case 3 : {return(Point*10); break;}
      case 5 : {return(Point*10); break;}
      default : return(Point);
     }

  } 
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double PipDiff(double a,double b)
  {
   double value;
   if(a<b) //El kell ker�lni a negativ sz�mokat.
     {
      value=(b-a)/vPoint;
     }
   else
     {
      value=(a-b)/vPoint;
     }
   return (value);
  }
//+------------------------------------------------------------------+
