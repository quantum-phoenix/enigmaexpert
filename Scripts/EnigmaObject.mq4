//+------------------------------------------------------------------+
//|                                                 EnigmaObject.mq4 |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
extern int   Len_Cn=50;                // Channel length (bars)
extern color Col_Cn=Orange;            // Channel color
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
   //ArrowCreate("Now1",1);
   HorizonLineCreate();
  }
//+------------------------------------------------------------------+
void ArrowCreate(string name,int trend)
  {
   if(ObjectFind(name)<0)
     {
      if(trend==1)
        {
         ObjectCreate(name,OBJ_ARROW_UP,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,5.0);
           }else if(trend==-1){
         ObjectCreate(name,OBJ_ARROW_DOWN,0,Time[0],Open[0]);
         ObjectSet(name,OBJPROP_COLOR,Red);
         ObjectSet(name,OBJPROP_WIDTH,6.0);

           }else{
         Alert("Hiba az ArrowCreate -ben!");
        }

     }
   else
     {
      ObjectDelete(name);
     }
  }
  void HorizonLineCreate()
  {
         string name = string(Time[0]);
         ObjectCreate(name,OBJ_VLINE,0,Time[0],0);
         ObjectSet(name,OBJPROP_COLOR,Green);
         ObjectSet(name,OBJPROP_WIDTH,3.0);
  }
//+------------------------------------------------------------------+
